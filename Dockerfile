FROM node:14
LABEL maintainer="it@bibliosansfrontieres.org"

EXPOSE 4200

WORKDIR /front

COPY --chown=node package-lock.json package-lock.json
COPY --chown=node package.json package.json

RUN npm install

COPY --chown=node . .

ENTRYPOINT ["npm", "start"]
