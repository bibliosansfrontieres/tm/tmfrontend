import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { post } from 'selenium-webdriver/http';
import { Process } from '../models/process.model'
import { Deploy, IDeploy } from '../models/deploy.model'
import { IOptionsValues } from 'selenium-webdriver/chrome';
import { ProcessService } from '../services/process.service'
import { Constants } from './constants';

@Injectable({
  providedIn: 'root'
})

export class DeployService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private httpClient:  HttpClient, private processService: ProcessService) {}

  autoDetectLease(): Promise<any> {
    return this.httpClient.get(`${Constants.TM_API_URL}/deploy/lease`).toPromise();
  }

  autoDetectLeaseWithContent(): Promise<any> {
    return this.httpClient.get(`${Constants.TM_API_URL}/deploy/lease-with-content`).toPromise();
  }
  startDeploy(process: Process) {
    return this.processService.startDeploy(process);
  }

}
