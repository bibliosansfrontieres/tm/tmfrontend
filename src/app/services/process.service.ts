import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { post } from 'selenium-webdriver/http';
import { Process, IProcess } from '../models/process.model'
import { Deploy } from '../models/deploy.model'
import { IOptionsValues } from 'selenium-webdriver/chrome';
import { Constants } from './constants';
import { ResponseType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class ProcessService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private httpClient:  HttpClient) {}

  getProcess(process_id: string): Promise<Process> {
    return  this.httpClient.get(`${Constants.TM_API_URL}/process/?id=` + process_id).map(
      resp => <Process> resp
      ).pipe(
       retry(1),
       catchError(this.handleError)
    ).toPromise();
  }

  getAllProcesses(): Promise<Process[]> {
    return  this.httpClient.get(`${Constants.TM_API_URL}/process/`).map(
      (resp) => <Process[]>resp
      ).pipe(
       retry(1),
       catchError(this.handleError)
    ).toPromise();
  }

  getProcessesByReceipts(receipts: number[]): Promise<Process[]> {
    let params = '';
    receipts.forEach(
      (receipt) => {
        params += '&id=' + receipt;
      }
    );
    return  this.httpClient.get(`${Constants.TM_API_URL}/process/recipe/?` + params).map(
      (resp) => <Process[]>resp
      ).pipe(
       retry(1),
       catchError(this.handleError)
    ).toPromise();
  }
  startBuild(process: Process, action: String): Promise<IProcess> {
    return this.httpClient.post(`${Constants.TM_API_URL}/gateway/` + action, JSON.stringify(process), this.httpOptions)
        .map((resp) => <IProcess>resp).toPromise()
    }

  startApprove(process: Process): Promise<IProcess> {
    return this.httpClient.post(`${Constants.TM_API_URL}/gateway/approve`, JSON.stringify(process), this.httpOptions)
        .map((resp) => <IProcess>resp).pipe(
          retry(1),
          catchError(this.handleError)
        ).toPromise();
  }

  startDeploy(process: Process): Promise<IProcess> {
    return this.httpClient.post(`${Constants.TM_API_URL}/gateway/deploy`, JSON.stringify(process), this.httpOptions)
        .map((resp) => <IProcess>resp).pipe(
          retry(1),
          catchError(this.handleError)
        ).toPromise();
  }

  handleError(error: any) {
   let errorMessage = '';
   if (error.error instanceof ErrorEvent) {
     // client-side error
     errorMessage = `Error: ${error.error.message}`;
   } else {
     // server-side error
     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
   }
   window.alert(errorMessage);
   return throwError(errorMessage);
 }
}
