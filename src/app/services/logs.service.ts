import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { Log, ILog } from '../models/log.model';
import { Constants } from './constants';

@Injectable({
  providedIn: 'root'
})

export class LogService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private httpClient:  HttpClient) {}

  getLogsByProcess(id: string): Promise<Log[]> {
    return  this.httpClient.get(`${Constants.TM_API_URL}/logs/?process_id=` + id).map((resp) => <ILog[]> resp).toPromise();
  }

  getHighestErrorByProcess(id: string): Promise<any> {
    return this.httpClient.get(`${Constants.TM_API_URL}/logs/?process_id=` + id + `&level=HIGHEST`).map((resp) => resp).toPromise();
  }

}
