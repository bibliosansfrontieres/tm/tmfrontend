import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { VirtualMachine, IVirtualMachine } from '../models/vm.model';
import { Constants } from './constants';

@Injectable({
  providedIn: 'root'
})

export class VMService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  constructor(private httpClient:  HttpClient) {}

  listVM(): Promise<VirtualMachine[]> {
    return  this.httpClient.get(`${Constants.TM_API_URL}/vm/list`).map((resp) => <IVirtualMachine[]> resp).toPromise();
  }

  RemoveVM(vm: VirtualMachine) {
    return this.httpClient.post(`${Constants.TM_API_URL}/vm/delete`, JSON.stringify(vm), this.httpOptions).toPromise();
  }

}
