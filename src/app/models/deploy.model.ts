import { Device } from './device.model';

export interface IDeploy {
  process_id: string;
  devices: Device[];
  description: string;
  parent_process_id: string;

}

export class Deploy implements IDeploy {
  process_id: string;
  devices: Device[];
  description: string;
  parent_process_id: string;
}
