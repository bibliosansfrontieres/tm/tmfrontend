export interface IProject {
  id: string;
  name: string;
  device: string;
  description: string;
  country: string;
  language: string;
  title: string;
}

export class Project implements IProject {
  id: string;
  name: string;
  device: string;
  description: string;
  country: string;
  language: string;
  title: string;
}
