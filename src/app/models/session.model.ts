import { User } from './user.model';
export class Session implements ISession {
  user: User;
  token: string;
}

export interface ISession {
  user: User;
  token: string;
}
