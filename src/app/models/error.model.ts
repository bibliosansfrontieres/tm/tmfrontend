export class Error implements IError {
  status: number;
  statusText: string;
}

export interface IError {
  status: number;
  statusText: string;
}
