import { MediaCenter } from './media_center.model';
import { Recipe } from './recipe.model';
import { Project } from './project.model';
import * as moment from 'moment';
import { User } from './user.model';
import { Approval } from './approval.model';
import { Deploy } from './deploy.model';

export interface IProcess {
  id: string;
  description: string;

  timestamp: string;
  image_url: string;
  media_center_configuration: MediaCenter;
  recipe: Recipe;
  project: Project;
  user: User;

  approval: Approval;
  deployment: Deploy;
}

export class Process implements IProcess {
  id: string;
  description: string;

  timestamp: string = moment().format(moment.HTML5_FMT.DATETIME_LOCAL);
  media_center_configuration = new MediaCenter();
  recipe = new Recipe();
  project = new Project();
  user: User;
  approval: Approval;

  deployment: Deploy;

  image_url: string = '';
}
