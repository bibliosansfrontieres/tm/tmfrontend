export interface IVirtualMachine {
    container_id: string;
    name: string;
    project_name: string;
    process_id: string;
    mediacenter: string;
    url: string;
    created_at: string;
    owner: string;
  }
  
  export class VirtualMachine implements IVirtualMachine {
    container_id: string;
    name: string;
    project_name: string;
    process_id: string;
    mediacenter: string;
    url: string;
    created_at: string;
    owner: string;
  }
  