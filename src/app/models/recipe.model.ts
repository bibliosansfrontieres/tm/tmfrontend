export class Recipe implements IRecipe {
  id: number;
  name: string;
  factories: string[];
}

export interface IRecipe {
  id: number;
  name: string;
  factories: string[];
}
