export interface IMediaCenter {
  engine: string;
  languages: string[];
}

export class MediaCenter implements IMediaCenter {
  engine: string = 'hugo';
  languages: string[] = ['fr'];
}

