export class Log implements ILog {
  process_id: string;
  log: string;
  date: string;
  level: string;
}

export interface ILog {
  process_id: string;
  log: string;
  date: string;
  level: string;
}
