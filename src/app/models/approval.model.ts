export interface IApproval {
  process_id: string;
  user: string;
  timestamp: string;
  approved: boolean;
  description: string;
  parent_process_id: string;
}

export class Approval implements IApproval {
  process_id: string;
  user: string;
  timestamp: string;
  approved: boolean;
  description: string;
  parent_process_id: string;
}
