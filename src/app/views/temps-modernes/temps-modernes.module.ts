// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// Para pasar valores en las funciones
import { FormsModule } from '@angular/forms';

// Para el Collapse
import { CollapseModule } from 'ngx-bootstrap/collapse';


// Para el Modal
import { ModalModule } from 'ngx-bootstrap/modal';

// import { ColorsComponent } from './colors.component';
// import { TypographyComponent } from './typography.component';

import { BuildComponent } from './build.component';
import { ApproveComponent } from './approve.component';
import { DeployComponent } from './deploy.component';
import { LogComponent } from './logs.component';
import { ResetComponent } from './reset.component';
import { VMComponent } from './vm.component';


// Theme Routing
import { TempsModernesRoutingModule } from './temps-modernes-routing.module';

@NgModule({
  imports: [
    CommonModule,
    TempsModernesRoutingModule,
    // para el control de collapse
    CollapseModule,
    // para las ventanas modales
    ModalModule.forRoot(),
    // para pasar variables en la funciones
    FormsModule
  ],
  declarations: [
    BuildComponent,
    ApproveComponent,
    DeployComponent,
    LogComponent,
    ResetComponent,
    VMComponent
  ]
})
export class TempsModernesModule { }
