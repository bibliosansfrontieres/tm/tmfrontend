import { Component, OnInit, ViewChild } from '@angular/core';
import { ProcessService } from '../../services/process.service';
import { Process, IProcess } from '../../models/process.model';

import { LogService } from '../../services/logs.service';
import { Log, ILog } from '../../models/log.model';

import { ModalDirective } from 'ngx-bootstrap/modal';

import { Constants } from '../../services/constants';


@Component({
  templateUrl: 'logs.component.html'
})
export class LogComponent implements OnInit {

  @ViewChild('logDetailsModal') logDetailsModal: ModalDirective;

  constructor(private processService: ProcessService, private logService: LogService) {
    this.getProcessList();
  }

  processList: Process[] = new Array;
  processErrorlevelList: any[] = new Array();

  process: Process = new Process();
  allProcesses: boolean = false;
  isCollapsed = false;

  textLogs: string;
  logs: Log[];

  functionalProcesses: number[] = [1, 4, 12];
  process_id: string = '';
  itemsPerPage: number = 10;
  itemsPage: Process[];

  omeka_url = Constants.OMEKA_ITEM_URL;

  applyFilters(event: any) {
    this.getProcessList();
  }

  collapsed(event: any): void {
   // console.log(event);
  }

  expanded(event: any): void {
   // console.log(event);
  }

  showDetail(id: string): void {
    this.textLogs = '';
    this.processService.getProcess(id).then(
      (process) => {
        this.process = <Process>process
        this.logService.getLogsByProcess(this.process.id).then(
          (logs) => {
            logs.forEach((log) => {
              this.textLogs += log.date + ': [' + log.level + '] - ' + log.log + '\n';
            });
            this.logDetailsModal.show();
          });
      });
  }

  getProcessList() {
    this.processList = new Array;
    if (this.allProcesses) {
      this.processService.getAllProcesses().then(
        (processes) => {
          this.processList = <Process[]>processes;
        }
      );

    } else if (this.process_id !== '') {
      this.processService.getProcess(this.process_id).then(
        (process) => {
          this.processList.push(<Process> process);
        }
      );
    } else {
      this.processService.getProcessesByReceipts(this.functionalProcesses).then(
        (processes) => {
          this.processList = <Process[]>processes;
        }
      );
    }
  }

  amountOfPages(): number {
    return Math.round(this.processList.length / this.itemsPerPage);
  }

  redirectToVM(url: string) {
    window.open(url, '_blank');
  }

  ngOnInit(): void {

  }
}
