import { Component, OnInit, ViewChild  } from '@angular/core';
import { ProcessService } from '../../services/process.service';
import { Process } from '../../models/process.model';
import { Error } from '../../models/error.model';
import * as moment from 'moment';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { SessionService } from '../../services/session.service';

@Component({
  templateUrl: 'build.component.html'
})
export class BuildComponent implements OnInit {
   @ViewChild('succefulModal') succefulModal: ModalDirective;
   @ViewChild('errorModal') errorModal: ModalDirective;

  templateSelect = false;

  project_name: string;

  a_process = new Process();

  public http_error: Error = new Error();

  public process =  new Process();

  process_type = '';

  receipt: string;

  project_name_value = '';

  country_value: string;

  device_value: string;

  constructor(private processService: ProcessService, private sessionService: SessionService) { }

  ngOnInit(): void {
    this.init();
  }

  startBuild(): void {
    // J'ajoute l'utilisateur depuis la session
    //this.a_process.project.name = this.a_process.project.name.toLowerCase();
    this.a_process.project.name = this.a_process.project.device + '-'
      + this.a_process.project.country + '-' + this.project_name;
    this.a_process.user = this.sessionService.get().user;
    this.a_process.timestamp = moment().format(moment.HTML5_FMT.DATETIME_LOCAL);
    this.processService.startBuild(this.a_process, this.receipt).then(
      (resp) => {
        this.process = resp;
        this.succefulModal.show();
        this.init();
      }
    ).catch((error) => {
      this.http_error.status = error.status;
      this.http_error.statusText = error.statusText;
      this.errorModal.show();
    });
  }

  formatErrorMsg(error: Error): string {
    return '(' + error.status + ') - ' + error.statusText;
  }

  showOptions(event: any): void {
     this.templateSelect = (event.target.value === 'visualize' || event.target.value === 'configure');
  }
  copyMessage(val: string) {
      const selBox = document.createElement('textarea');
      selBox.style.position = 'fixed';
      selBox.style.left = '0';
      selBox.style.top = '0';
      selBox.style.opacity = '0';
      selBox.value = val;
      document.body.appendChild(selBox);
      selBox.focus();
      selBox.select();
      document.execCommand('copy');
      document.body.removeChild(selBox);
      alert('Process Id copied to Clipboard');
  }

  init(): void {
    this.a_process = new Process();
    this.project_name = '';
    this.receipt = 'prepare';
    this.a_process.project.country = 'fra';
    this.a_process.project.device = 'idc';
    this.country_value = this.a_process.project.country + '-';
    this.device_value = this.a_process.project.device + '-';
  }

  onProjectKeyUp(event: any) {
    this.project_name_value = event.target.value;
  }

  onCountryClick(event: any) {
    this.country_value = event.target.value + '-';
  }

  onDeviceClick(event: any) {
    this.device_value = event.target.value + '-';
  }
  remove_underscore(event) {
      var k;
      k = event.charCode; 
      return ((k > 32 && k < 95) || (k > 96 && k < 123));
  }
}

