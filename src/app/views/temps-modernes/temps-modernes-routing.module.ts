import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { ColorsComponent } from './colors.component';
// import { TypographyComponent } from './typography.component';

import { BuildComponent } from './build.component';
import { ApproveComponent } from './approve.component';
import { DeployComponent } from './deploy.component';
import { LogComponent } from './logs.component';
import { ResetComponent } from './reset.component';
import { VMComponent } from './vm.component';



const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Temps Modernes'
    },
    children: [
      {
        path: 'manage',
        component: BuildComponent,
        data: {
          title: 'Manage'
        }
      },
      {
        path: 'approve',
        component: ApproveComponent,
        data: {
          title: 'Approve'
        }
      },
      {
        path: 'deploy',
        component: DeployComponent,
        data: {
          title: 'Deploy'
        }
      },
      {
        path: 'logs',
        component: LogComponent,
        data: {
          title: 'Log'
        }
      },
      {
        path: 'reset',
        component: ResetComponent,
        data: {
          title: 'Reset'
        }
      },
      {
        path: 'vm',
        component: VMComponent,
        data: {
          title: 'Virtual Machines'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TempsModernesRoutingModule {}
