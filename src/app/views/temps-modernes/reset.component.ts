import { Component, OnInit } from '@angular/core';
import { Device } from '../../models/device.model';
import { DeployService } from '../../services/deploy.service';

//Pour tester le service d'utilisateur
import { UserService } from '../../services/user.service';
//

@Component({
  templateUrl: 'reset.component.html'
})
export class ResetComponent implements OnInit {


  public device_list: Device[] = [
    // {id: 1, name: 'asdasdasasdasdasdd'},
    // {id: 2, name: 'asdasdasasdasdasdd'}
  ];

  public processModal;

  _device: string;
  selectedDevice: string;

  ngOnInit(): void {
  }

  addDeviceToList() {
    this.device_list.push(
      {
        //id: this.device_list.length + 1,
        //name: this._device,
        mac_address: this._device,
        project_name: this._device
      }
    );
    //console.log(this.device);
    //console.log(this.device_list.length);
    this._device = '';
  }

  removeDeviceFromList(deviceId: number) {
    for (let i = 0; i < this.device_list.length; i++) {

      // tslint:disable-next-line:triple-equals
      if (this.device_list[i]['id'] == deviceId) {
        this.device_list.splice(i, 1);
      }
    }
  }
  autoDetectDevices() {
    this.deployService.autoDetectLease().then(
      (resp) => {
        //il faut valider qu'il n'y ait pas des doublons
        //je vais utiliser le .find
        resp[0]['arguments']['leases'].forEach(
          lease => {
            this.device_list.push(
              {
                //id: this.device_list.length + 1,
                //name: lease['hw-address'],
                 mac_address: lease['hw-address'],
                  project_name: lease['hostname']
              }
            );
          }
        );
      }
    );
  }

  cancel() {
    //this.userService.setToken('test1');
    //alert(this.userService.getToken());
    //console.log(this.userService.getToken());
  }

  clearAllList() {
    this.device_list = [];
  }
  constructor(private deployService: DeployService, private userService: UserService) {
  }


}
