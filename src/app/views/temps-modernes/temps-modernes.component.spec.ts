import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempsModernesComponent } from './temps-modernes.component';

describe('TempsModernesComponent', () => {
  let component: TempsModernesComponent;
  let fixture: ComponentFixture<TempsModernesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempsModernesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempsModernesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
