import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Validators } from '@angular/forms';
import { Device } from '../../models/device.model';
import { ProcessService } from '../../services/process.service';
import { DeployService } from '../../services/deploy.service';
import { Process } from '../../models/process.model';
import { Deploy } from '../../models/deploy.model';
import { SessionService } from '../../services/session.service';
import { LoginService } from '../../services/login.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  templateUrl: 'deploy.component.html'
})

export class DeployComponent implements OnInit {
  constructor(private deployService: DeployService,
    private processService: ProcessService,
    private sessionService: SessionService,
    private loginService: LoginService) { }

  @ViewChild('detailsModal') detailsModal: ModalDirective;
  process_id: string = '';
  device_list: Device[] = [];

  deploy = new Deploy();
  process = new Process();
  selectedProcess = new Process();
  //processModal;

  validRecipes: number[] = [4, 12];


  _device: string;
  selectedDevice: Device;

  ngOnInit(): void {
  }

  reset(input: any): void {
    input = '';
  }

  private sessionError(err: any) {
    return err;
  }

  startDeploy(process_id: string) {
    this.processService.getProcess(process_id).then(
      (process) => {
        if (this.validateBeforeDeploy(process)) {
          this.deploy.process_id = process.id;
          this.deploy.devices = this.device_list;
          process.deployment = this.deploy;
          process.user = this.sessionService.get().user;
          //process.id = null;
          this.deployService.startDeploy(process).then(
            () => {
              try {
                alert('The deploy for process ' + this.process_id + ' was started!. See the logs.');
              } catch (error) {
                alert('The deploy for process ' + this.process_id + ' could not be started due to: ' + error + '. See the logs.');
              }
            }
          );
        }
      }
    );
  }

  addDeviceToList() {
    if (this.device_list.find(device => (device.mac_address === this._device)) === undefined) {
      this.device_list.push(
        {
          mac_address: this._device,
          project_name: null
        }
      );
      this._device = '';
    } else {
      alert('This device is already in the list');
    }
  }

  removeDeviceFromList(deviceId: string) {
    for (let i = 0; i < this.device_list.length; i++) {
      // tslint:disable-next-line:triple-equals
      if (this.device_list[i]['mac_address'] == deviceId) {
        this.device_list.splice(i, 1);
      }
    }
  }
  autoDetectDevices() {
    this.deployService.autoDetectLeaseWithContent().then(
      (resp) => {
        resp[0]['arguments']['leases'].forEach(
          (lease: any) => {
            if (this.device_list.find(device => (device.mac_address === lease['hw-address'])) === undefined) {
                  this.device_list.push(
                {
                  mac_address: lease['hw-address'],
                  project_name: lease['project_name']
                }
              );
            }
          }
        );
      }
    );
  }

  resetAllFields(): void {
    this.clearAllList();
    this.resetProcessId();
  }
  clearAllList() {
    this.device_list = [];
  }

  resetProcessId(): void {
    this.process_id = '';
  }

  validateMacAddress(mac_address: string) {
    //return Validators.pattern()
  }

  validateBeforeDeploy(process: Process): Boolean {
    //const deployable_process = [4, 9, 12];
    try {
      if (process.id === undefined) {
        throw new Error('Process does not exist!');
      } else if (!this.validRecipes.includes(process.recipe.id)) {
        window.alert('Error: This kind of process (' + process.recipe.name + ') is not deployable! I need a Visualize process_id!');
        return false;
      } else if (!process.approval.approved) {
        window.alert('Error: This process has not been approved!');
        return false;
      }
      //console.log(deployable_process.includes(this.selectedProcess.recipe.id));
      //Todo: Validate approval
    } catch (error) {
      alert(error);
      return false;
    }
    return true;
  }

  public lookup(process_id: string) {
    this.processService.getProcess(process_id).then(
      (process) => {
        try {
          if (process.id === undefined) {
            throw new Error('Process does not exist!');
          }
          this.selectedProcess = process;
          this.detailsModal.show();
        } catch (error) {
          alert(error);
        }
      }
    );
  }

  redirectToVM(url: string) {
    window.open(url, '_blank');
  }


}
