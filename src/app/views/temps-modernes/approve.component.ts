import { Component, OnInit, ViewChild } from '@angular/core';
import { Process } from '../../models/process.model';
import { Approval } from '../../models/approval.model';
import { ProcessService } from '../../services/process.service';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { SessionService } from '../../services/session.service';

@Component({
  templateUrl: 'approve.component.html'
})

export class ApproveComponent implements OnInit {
  //public myModal;
  @ViewChild('detailsModal') detailsModal: ModalDirective;


  approval: Approval = new Approval();
  process: Process = new Process();
  process_id: string;
  validRecipes: number[] = [4];
  ngOnInit(): void {
  }

  constructor(private processService: ProcessService, private sessionService: SessionService) { }

  public lookup(process_id: string) {
    this.processService.getProcess(process_id).then(
      (resp) => {
        try {
          if (resp.id !== undefined) {
            this.process = resp;
            this.detailsModal.show();
          } else {
            throw new Error('Process does not exist!');
          }
        } catch (error) {
          alert(error);
        }
      }
    );
  }

  validate(process: Process): boolean {
    return this.validRecipes.find((recipe) => recipe === process.recipe.id) !== undefined;
  }

  public approve(approve: boolean) {
    this.processService.getProcess(this.process_id).then(
      (resp) => {
        try {
          if (resp.id === undefined) {
            throw new Error('Process does not exist!');
          } else if (!this.validate(resp)) {
            throw new Error('This kind of process cannot be approved/rejected!');
          }
          this.process = resp;
          this.process.user = this.sessionService.getLoggedUser();
          this.process.approval.process_id = this.process.id;
          this.process.approval.description = 'Approved from the front';
          this.process.approval.approved = approve;
          this.processService.startApprove(this.process).then((approval) => {
            status = (approve === true ? 'approved' : 'rejected');
            alert('The process ' + approval.approval.parent_process_id + ' was ' + status + '!');
          });
        } catch (error) {
          alert(error);
        }
      }
    );
  }

  redirectToVM(url: string) {
    window.open(url, '_blank');
  }

}
