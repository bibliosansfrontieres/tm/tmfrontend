import { Component, OnInit, ViewChild } from '@angular/core';
import { VirtualMachine } from '../../models/vm.model';
import { VMService } from '../../services/vm.service';

import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  templateUrl: 'vm.component.html'
})

export class VMComponent implements OnInit {
  //public myModal;
  @ViewChild('VMModal') VMModal: ModalDirective;


  vm: VirtualMachine = new VirtualMachine();
  VMList: VirtualMachine[] = new Array;
 
  a_vm = new VirtualMachine();

  ngOnInit(): void {
  }

  constructor(private vmService: VMService) {
    this.getVMList();
  }

  getVMList() {
    this.VMList = new Array;
    this.vmService.listVM().then(
        (vms) => {
          this.VMList = <VirtualMachine[]>vms;
        }
    );
  }

  convertToPrettyDate(date: string) {
    var event = new Date(date);
    return event.toLocaleString('fr-FR', { timeZone: 'Europe/Paris' })
  }

  redirectToVM(url: string) {
    window.open(url, '_blank');
  }

  DeleteVM(container_id: string, process_id: string) {
    this.a_vm.container_id = container_id
    this.a_vm.process_id = process_id
    if (window.confirm('Are you sure you want to delete VM ' + process_id + ' ?')) {
        this.vmService.RemoveVM(this.a_vm).then(
            () => {
                this.getVMList()
            }
        )
    }
  }
}
